﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class Shooting : MonoBehaviourPunCallbacks
{
    public Camera camera;
    public GameObject hitEffefctPrefab;

    [Header("Hp related")]
    public Image hpBar;
    public float startHp = 100;
    private float hp;

    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        hp = startHp;
        hpBar.fillAmount = hp / startHp;
        animator = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
           
    }
    public void fire()
    {
        RaycastHit hit;
        Ray ray = camera.ViewportPointToRay(new Vector3(.5f, .5f));

        if (Physics.Raycast(ray, out hit, 200))
        {
            Debug.Log(hit.collider.gameObject.name);

            photonView.RPC("CreateHitEffects", RpcTarget.All,hit.point);

            if (hit.collider.gameObject.CompareTag("Player")&&!hit.collider.gameObject.GetComponent<PhotonView>().IsMine)//call rpc
            {
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 25);
            }
        }
    }
    [PunRPC]
    public void TakeDamage(int dmg,PhotonMessageInfo info)
    {
        GameObject killFeed = GameObject.Find("KillFeed");
        this.hp -= dmg;
        this.hpBar.fillAmount = hp / startHp;

        if (hp <= 0)
        {
            Die();
            Debug.Log(info.Sender.NickName + " killed " + info.photonView.Owner.NickName);
            killFeed.GetComponent<Text>().text = info.Sender.NickName + " Killed " + info.photonView.Owner.NickName;//killfeed ui
        }
    }
    [PunRPC]
    public void CreateHitEffects(Vector3 pos)
    {
        GameObject hitEffefctGameObject = Instantiate(hitEffefctPrefab, pos, Quaternion.identity);
        Destroy(hitEffefctGameObject, .2f); 
    }

    public void Die()
    {
        if (photonView.IsMine)
        {
            animator.SetBool("isDead", true);
            StartCoroutine(RespawnCountdown());
        }
    }
    IEnumerator RespawnCountdown()//10 kills
    {
        GameObject resText = GameObject.Find("Respawn Text");

        float resTime = 5.0f;
        while (resTime>0)
        {
            yield return new WaitForSeconds(1.0f);
            resTime--;
            transform.GetComponent<PlayerMovement>().enabled = false;
            resText.GetComponent<Text>().text = "dead. Repawing in " + resTime.ToString(".00"); 
        }
        animator.SetBool("isDead", false);
        resText.GetComponent<Text>().text = "";

        int randomPointX = Random.Range(-10, 10);
        int randompointZ = Random.Range(-10, 10);
        this.transform.position = new Vector3(randomPointX, 0, randompointZ);
        //transform.GetComponent<GameManager>().enabled = true;
        transform.GetComponent<PlayerMovement>().enabled = true;
        photonView.RPC("regainHP", RpcTarget.AllBuffered);
    }
    
    [PunRPC]
    public void regainHP()
    {
        hp = 100;
        hpBar.fillAmount = hp / startHp;
    }
}
