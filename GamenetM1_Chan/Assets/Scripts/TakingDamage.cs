﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class TakingDamage : MonoBehaviourPunCallbacks
{
    [SerializeField] Image hpBar; 
    private float startHp = 100;
    public float hp;
    // Start is called before the first frame update
    void Start()
    {
        hp = startHp;
        hpBar.fillAmount = hp / startHp;
    }
     [PunRPC]
    public void TakeDmg(int dmg)
    {
        hp -= dmg;
        hpBar.fillAmount = hp / startHp;
        Debug.Log(hp);
        if (hp<0)
        {
            Die();
        }
    }
    private void Die()
    {
        if (photonView.IsMine)
        {
            GameManager.instance.LeaveRoom();
        }       
    }
}
